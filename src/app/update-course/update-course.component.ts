import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Course } from '../course';
import { CourseService } from '../course.service';
import { NotificationService } from '../notification.service';


@Component({
  selector: 'app-update-course',
  templateUrl: './update-course.component.html',
  styleUrls: ['./update-course.component.css']
})
export class UpdateCourseComponent implements OnInit {
  id: number;
  course : Course  = new Course();

  constructor(private courseService: CourseService,private router: Router,private route: ActivatedRoute,private notifyService : NotificationService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    this.courseService.getCourseById(this.id).subscribe(data =>{
      this.course = data;
    }, error => console.log(error));
  }
  updateCourse(){
    this.courseService.updateCourse(this.id,this.course).subscribe(data => {
      console.log(data);
      this.course = new Course();
      this.goToCourseList();
    }, error =>{
      this.showToasterWarning();
      console.log(error)
      

    } );
  }
  onSubmit(){
    this.courseService.updateCourse(this.id,this.course).subscribe( data =>{
      this.showToasterSuccess();
      this.goToCourseList();
    }
    , error => console.log(error));
  }
  goToCourseList(){
    this.router.navigate(['/courses']);

  }
  showToasterSuccess(){
    this.notifyService.showSuccess("Course Updated !!", "Success")
}
showToasterWarning(){
  this.notifyService.showWarning("This is warning !!", "Warning")
}
}
