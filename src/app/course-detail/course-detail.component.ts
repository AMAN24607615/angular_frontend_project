import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from '../course';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {

  id: number;
  course: Course;
  constructor(private route: ActivatedRoute,private courseService: CourseService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']; 

    this.course = new Course();
    this.courseService.getCourseById(this.id).subscribe( data => {
      this.course = data;
    })
  }


}
