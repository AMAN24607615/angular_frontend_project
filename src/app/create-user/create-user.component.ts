import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  user: User = new User();
  public userError: User;
  startDate = new Date(1950, 0, 1);

  constructor(private userService: UserService,private router:Router) { }

  private isSaved:boolean=false;
  private UserExist:boolean=false;

  ngOnInit(): void {
  }
  saveUser(){
    this.userService.createUser(this.user).subscribe(data =>{
      console.log(data);
      this.goToUserList();
      this.user = new User();
      this.isSaved=true;
      this.UserExist=false;
    },
    error =>{
      this.userError = error.error;
      this.isSaved=false;
      if(error.status==409){
        this.isSaved=false;
        this.UserExist=true;
      }
      console.log(error);
    }
    

    )
  }
  goToUserList(){
    this.router.navigate(['/users']);
  }
  onSubmit(){
    console.log(this.user);
    this.saveUser();
  }

}
