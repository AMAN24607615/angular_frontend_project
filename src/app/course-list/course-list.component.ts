import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from '../course';
import { CourseService } from '../course.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  courses: Course[];

  constructor(private courseService : CourseService, 
    private router: Router,private notifyService : NotificationService) { }

  public titleStyle = {
      color: "orange",
      fontStyle:"bold",
      textAlign: "center"
  }
  ngOnInit(): void {
    this.getCourses();
  }

  private getCourses(){
    this.courseService.getCourseList().subscribe(data => {
      this.courses = data;
      this.showToasterInfo();
    })
  }

  courseDetail(id: number){
    this.router.navigate(['course-detail',id]);

  }
  updateCourse(id: number){
      this.router.navigate(['update-course',id]);
  }
  showToasterSuccess(){
    this.notifyService.showSuccess("Course Updated !!", "Success")
   }
   showToasterInfo(){
    this.notifyService.showInfo("Course List!!", "Information")
 }
 
  deleteCourse(id: number){
    this.courseService.deleteCourse(id).subscribe(data =>{
      console.log(data);
      this.getCourses();
      this.showToasterDelete();
    });
  }
  showToasterDelete(){
    this.notifyService.showWarning("Course Deleted !!", "Success")
  }
}
