import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})

export class UserLoginComponent implements OnInit {

  users:User  = new User();
  constructor(private userservice : UserService) { }
  ngOnInit(): void {
   
  }

  userLogin(){
    console.log(this.users);
    alert("You logged successfully");
    this.userservice.login_User(this.users).subscribe(data=>{
      alert("You logged successfully");
    },error=>{
      alert("Please enter correct email and password");
    });
    
  }
  

}
