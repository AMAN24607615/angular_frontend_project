import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];

  constructor(private userService:UserService,private router:Router) { }

  public titleStyle = {
      color: "orange",
      fontStyle:"bold",
      textAlign: "center"
  }

  ngOnInit(): void {
    this.getUsers();
  }
  private getUsers(){
    this.userService.getUserList().subscribe(data => {
      this.users = data;
      console.log(this.users);
    })
  }
  updateUser(id: number){
    this.router.navigate(['update-user',id]);
 }

 userDetail(id: number){
  this.router.navigate(['user-detail',id]);

}



}
