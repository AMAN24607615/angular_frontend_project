 export class User {
    id:number;
    name:string;
    email:string;
    dob:Date;
    phone:string;
    address:string;
    gender:string;
    password:string
}
