import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from './course';


@Injectable({
  providedIn: 'root'
})
export class CourseService {

  private baseUrl = "http://localhost:8080/courses";
  constructor(private httpClient: HttpClient) { }

  getCourseList(): Observable<Course[]>{
    return this.httpClient.get<Course[]>(`${this.baseUrl}`)
  }

  createCourse(course: Course): Observable<Object>{
    return this.httpClient.post(`${this.baseUrl}`,course);
  }

  getCourseById(id:Number): Observable<Course>{
    return this.httpClient.get<Course>(`${this.baseUrl}/${id}`);
  }
  updateCourse(id: number, course: Course): Observable<Object>{
    return this.httpClient.put(`${this.baseUrl}/${id}`, course);
  }
  deleteCourse(id: number):Observable<object>{
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }
}
