import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from '../course';
import { CourseService } from '../course.service';
import { NotificationService } from '../notification.service';




@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css']
})
export class CreateCourseComponent implements OnInit {
  course: Course  = new Course();
  public courseError: Course;

  private isSaved:boolean=false;
  private courseExist:boolean=false;
  startDate = new Date(1990, 0, 1);
  //notifyService: any;


  constructor(private courseService : CourseService, private router: Router, private notifyService : NotificationService) { }

  ngOnInit(): void {
    
  }

  
  saveCourse(){
    this.courseService.createCourse(this.course).subscribe(
      data => {
        console.log(data);
        this.showToasterWarning();
        this.gotToCourseList();
        this.course = new Course();
        this.isSaved = true;
        this.courseExist = false;
        this.courseError = new Course();
        this.showToasterSuccess();
        
       

    },
    error =>{
      this.courseError = error.error;
      this.isSaved = false;
      this.showToasterError();
      if(error.status==409){
        this.isSaved = false;
        this.courseExist = true;
        //this.showToasterError();
      }
      console.log(error);


    })
    
  }
  gotToCourseList(){
    this.router.navigate(['/courses']);
  }
  onSubmit(){
    console.log(this.course);
    this.saveCourse();
   
  }
  showToasterSuccess(){
    this.notifyService.showSuccess("New Course Created !!", "Success")
}

showToasterError(){
    this.notifyService.showError("Something is wrong !!", "Error")
}

showToasterInfo(){
    this.notifyService.showInfo("This is info !!", "Information")
}

showToasterWarning(){
    this.notifyService.showWarning("Don't Fill Empty Form !!", "Warning")
}
showToasterDelete(){
  this.notifyService.showInfo("Course Deleted !!", "Success")
}

}
