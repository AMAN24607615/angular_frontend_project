export class Course {
    id: number;
    name: string;
    author: string;
    dop: Date;
    email: String;
    noc: number;
    active:boolean;
    phone:string;

}

