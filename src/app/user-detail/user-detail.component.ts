import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  id: number;
  users: User;
  constructor(private route: ActivatedRoute,private userService: UserService) { }


  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']; 

    this.users = new User();
    this.userService.getUserById(this.id).subscribe( data => {
      this.users = data;
    })
  }

}
