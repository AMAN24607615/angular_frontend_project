import { NgModule } from "@angular/core";
import { Route, RouterModule, Routes } from "@angular/router";
import { CourseDetailComponent } from "./course-detail/course-detail.component";
import { CourseListComponent } from "./course-list/course-list.component";
import { CreateCourseComponent } from "./create-course/create-course.component";
import { CreateUserComponent } from "./create-user/create-user.component";
import { UpdateCourseComponent } from "./update-course/update-course.component";
import { UpdateUserComponent } from "./update-user/update-user.component";
import { UserDetailComponent } from "./user-detail/user-detail.component";
import { UserListComponent } from "./user-list/user-list.component";
import { UserLoginComponent } from "./user-login/user-login.component";


const routes: Routes = [
    {path: 'courses',component: CourseListComponent},
    {path: 'create-course',component: CreateCourseComponent},
    {path: '', redirectTo: 'courses',pathMatch: 'full'},
    {path: 'update-course/:id', component: UpdateCourseComponent },
    {path: 'course-detail/:id',component: CourseDetailComponent},
    //user routes
    {path: 'users',component: UserListComponent},
    {path: 'create-user', component: CreateUserComponent},
    {path: 'update-user/:id', component: UpdateUserComponent },
    {path: 'user-detail/:id',component: UserDetailComponent},
    {path: 'user-login',component: UserLoginComponent}


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]

})
export class AppRoutingModule{}